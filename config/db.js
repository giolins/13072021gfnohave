const mysql = require('mysql');

const connection = mysql.createConnection({
    host: '10.8.6.212', // O host do banco. Ex: localhost
    user: 'teste', // Um usuário do banco. Ex: user 
    password: 'teste', // A senha do usuário. Ex: user123
    database: 'gfnohave' // A base de dados a qual a aplicação irá se conectar, deve ser a mesma onde foi executado o Código 1. Ex: node_mysql
});

module.exports = connection