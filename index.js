const express =  require("express");
const app = express();
const multer = require("multer");
const path = require("path")
const fs = require('fs')
const ofx = require('ofx-convertjs');
const movimento = require('./model/movimentos')

const itens = extrato()
//console.log(itens)
  
//movimento.adicionar(itens,'2021-06',2,3,5)
app.set('view engine','ejs');

const storage = multer.diskStorage({
    destination:(req, file,cb)=>{
        cb(null, "uploads/");
    },
    filename:(req, file,cb)=>{
        cb(null, file.originalname + Date.now() + Path.extname(file.originalname));
    }
})
const upload = multer({storage})

app.get("/", (req, res) => {
    res.render('index', itens)
})

app.get('/extrato', (_req, res) => {
    res.send(extrato())
  })
  
app.post("/upload",upload.single("file"),(req,res)=>{
    res.send("Arquivo recebido");
}
)

app.listen(3000,()=>{
    console.log("servidor na porta 3000")
});

function extrato(){
    const file = fs.readFileSync('uploads\\cartao 05-2020.ofx', 'Latin1')
    const data = ofx.toJson(file);
    extratoContaJson = data['OFX']['BANKMSGSRSV1']['BANKTRANLIST']['STMTTRN']
    extratoContaJson.map((y)=>{y.DTPOSTED = (y.DTPOSTED.slice(0,8))})
    //console.log(extratoContaJson)
    return extratoContaJson
}
